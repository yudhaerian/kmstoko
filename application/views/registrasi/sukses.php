 <!-- Cart -->
<section class=" bgwhite p-t-70 p-b-100">
<div class="container">
<!-- Cart item -->
<div class="pos-relative">
<div class="bgwhite">
	<h1><?php echo $title ?></h1><hr>
	<div class="clearfix"></div>
	<br>

	<?php if($this->session->flashdata('sukses')) {
		echo '<div class="alert alert-warning">';			
		echo $this->session->flashdata('sukses');
		echo '</div>';
	} ?>

	<p class=" alert alert-success"> Registrasi telah dilakukan. <a href=" <?php echo base_url('masuk') ?> " class="btn btn-outline-success"><b> Login di sini gan! </b></a>. Anda juga bisa melakukan checkout <a href=" <?php echo base_url('belanja/checkout') ?> " class="btn btn-outline-warning"><i class="fa fa-shopping-cart"></i> <b> Check Out </b></a>.</p>


</div>
</div>
</div>
</section>